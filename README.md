# Boilerplate to download the Quickdraw Dataset #

Ref: Google's
[Quick,Draw!](https://github.com/googlecreativelab/quickdraw-dataset)
Datasets

To (download and) create the dataset, clone this
repository; descend to the folder, and:

```sh
make
```


To compute the dataset drawing indices

```sh
make indices.json.gz
```

## Drawing Indices ##

Drawing indices provide random access to each drawing
in the dataset. Using a json of the following format,

```json
{
  "key0": {"offset00","...","offset0N0"},
  "key1": {"offset10","...","offset1N1"},
  "...": "...",
  "keyM": {"offsetM0","...","offsetMNm"}
}
```

Given a pair `(key,offset)`, the drawing may be
accessed at using file handle, as follows:

```python
with open(f'bin/{key}.bin', 'rb') as F :
    F.seek(offset, 0)
```

Further the drawing may be read as described on the
[Quick,Draw!
website](https://github.com/googlecreativelab/quickdraw-dataset/blob/master/examples/binary_file_parser.py),
duplicated here for convenience.

```python
def unpack_drawing(file_handle):
  key_id, = unpack('Q', file_handle.read(8))
  country_code, = unpack('2s', file_handle.read(2))
  recognized, = unpack('b', file_handle.read(1))
  timestamp, = unpack('I', file_handle.read(4))
  n_strokes, = unpack('H', file_handle.read(2))
  image = []
  for i in range(n_strokes):
    n_points, = unpack('H', file_handle.read(2))
    fmt = str(n_points) + 'B'
    x = unpack(fmt, file_handle.read(n_points))
    y = unpack(fmt, file_handle.read(n_points))
    image.append((x, y))

  return {
    'key_id': key_id,
    'country_code': country_code,
    'recognized': recognized,
    'timestamp': timestamp,
    'image': image
  }
```
