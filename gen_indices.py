import logging as lg

## ====================================================
def main() :
  import json
  import time
  from pathlib import Path
  from datetime import timedelta as delT
  from multiprocessing import Process, Manager, Pool

  parser = cli()
  args = parser.parse_args()

  if args.verbose : lg.getLogger().setLevel(lg.DEBUG)
  lg.debug('Args: %s', args)

  bin_files = list(args.bin_folder.glob('*.bin'))
  keys = [
    Path(filename.name).stem
    for filename in bin_files
  ]

  t = time.time()

  with Pool(processes=args.num_workers) as pool :
    indices = pool.map(qdr_indices, bin_files)

  t = delT(seconds = (time.time() - t))
  lg.info(f'Indexed in {t}')

  t = time.time()
  Q = dict(zip(keys, indices))

  if args.output :
    with args.output.open('w') as J :
      json.dump(Q, J)

    t = delT(seconds = (time.time() - t))
    lg.info(f'Written to {args.output} in {t}.')

  else :
    print(json.dumps(Q))

## ====================================================
def cli() :
  import argparse
  from pathlib import Path

  parser = argparse.ArgumentParser(
    formatter_class=
    argparse.ArgumentDefaultsHelpFormatter,
    description='Generate drawing indices '
    'for Quickdraw dataset',
  )

  parser.add_argument(
    "-v", "--verbose", action='store_true',
    help="Verbose Logging."
  )

  parser.add_argument(
    "bin_folder", default="bin", type=Path,
    help="Quickdraw binary folder",
  )

  parser.add_argument(
    '-o', "--output", default="indices.json",
    type=Path, help="Output json filename",
  )

  parser.add_argument(
    '-w', "--num-workers", default=12,
    type=int, help="Num parallel procs",
  )

  return parser

## ====================================================
def qdr_indices(file) :
  from pathlib import Path
  import time
  from datetime import timedelta as delT

  with open(file, 'rb') as F :
    t = time.time()
    P = [F.tell()]
    while qdr_dwg_skip(F) :
      P.append(F.tell())

    t = delT(seconds = time.time() - t)
    lg.debug(f'{file}: EOF: {F.tell()} in {t}')

  return P

## ====================================================
def qdr_dwg_skip(file_handle) :
  from struct import unpack, error as StructError
  F = file_handle

  try :

    F.seek(15, 1)
    n, = unpack('H', F.read(2))

    for _ in range(n) :
      m, = unpack('H', F.read(2))
      F.seek(2*m, 1)

    return F.tell()

  except StructError as e :
    pass

if __name__ == '__main__' :
  lg.basicConfig(
    level=lg.INFO,
    format='%(levelname)-8s: %(message)s'
  )

  main()

