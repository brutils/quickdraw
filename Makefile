SHELL := /usr/bin/zsh

SPC :=
SPC += 

CAT_URL := https://raw.githubusercontent.com
CAT_URL := ${CAT_URL}/googlecreativelab/quickdraw-dataset
CAT_URL := ${CAT_URL}/master/categories.txt

CATEGORIES := $(shell curl -sL '${CAT_URL}' | tr ' ' '_')
STORE := http://storage.googleapis.com/quickdraw_dataset/full/binary
url_from_cat = ${STORE}/$(subst _,%20,$1).bin
BIN_FILES := ${CATEGORIES:%=bin/%.bin}

all : ${BIN_FILES}

bin/%.bin : bin
	curl -sL '$(call url_from_cat,$*)' -o $@

bin :
	mkdir -p $@

indices.json : bin
	python -m gen_indices -v $< -o $@

%.gz : %
	gzip -c $< > $@
